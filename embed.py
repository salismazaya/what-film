import os, random
from glob import glob
import os
import requests as r
import base64, cv2
from concurrent.futures import ThreadPoolExecutor
from glob import glob
import pymysql, json
import pymysql.cursors

files = glob('videos/*.mp4') + glob('videos/*.mkv')


def split_video_to_images(input_video_path, output_folder, name, interval_seconds=1):
    cap = cv2.VideoCapture(input_video_path)

    # Get the frames per second (fps) of the video
    fps = cap.get(cv2.CAP_PROP_FPS)

    # Calculate the frame interval
    frame_interval = int(fps * interval_seconds)

    # Create output folder if it doesn't exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Process each frame at the specified interval
    frame_number = 0
    while True:
        # Read the next frame
        ret, frame = cap.read()

        # Break the loop if no more frames are available
        if not ret:
            break

        # Save the frame as an image at the specified interval
        if frame_number % frame_interval == 0:
            output_path = os.path.join(output_folder, f"{name}_{frame_number // frame_interval}.png")
            width, height = 640, 360
            frame = cv2.resize(frame, (width, height))
            cv2.imwrite(output_path, frame)

        frame_number += 1

    # Release the video capture object
    cap.release()

def predict(path_to_image: str = None):
    image = base64.b64encode(open(path_to_image, 'rb').read()).decode()
    payload = {"image": image}
    response = r.post(
        'http://127.0.0.1:8000/embedding', json = payload
    )
    rv = response.json()
    return rv

import traceback
def up(path):
    try:
        name = path.removesuffix('.png').split('/')[-1].strip()
        output = predict(path)
        connection = pymysql.connect(host='localhost',
                             user='root',
                             password='kmzway87aa',
                             database='what_film',
                             cursorclass=pymysql.cursors.DictCursor)
        with connection:
            with connection.cursor() as cursor:
                sql = "INSERT INTO `data` (`title`, `vector`) VALUES (%s, %s)"
                cursor.execute(sql, (name, json.dumps(output)))
            
            connection.commit()

        print(name)
    except:
        traceback.print_exc()

for file in files:
    folder = 'output/' + str(random.randint(0, 10000))
    name = file.removesuffix('.mkv').removesuffix('.mp4').split("/")[-1]
    print(name, file)
    split_video_to_images(file, folder, name)

    images = glob(f'{folder}/*.png')
    with ThreadPoolExecutor(max_workers = 30) as t:
        t.map(up, images)
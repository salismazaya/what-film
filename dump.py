import pinecone
import pymysql, pymysql.cursors, json
from concurrent.futures import ThreadPoolExecutor

pinecone.init(
    api_key = '10119962-3d62-4e4e-91ea-8bd1ed73d774',
    environment = 'us-central1-gcp'
)

index = pinecone.Index('what-index')

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='kmzway87aa',
                             database='what_film',
                             cursorclass=pymysql.cursors.DictCursor)


with connection:
    with connection.cursor() as cursor:
        sql = "SELECT * FROM data"
        cursor.execute(sql)
        def keren(data):
            print(data['title'])
            index.upsert(
                vectors = [
                    (
                        data['title'],
                        json.loads(data['vector']),
                    )
                ]
            )

        with ThreadPoolExecutor(max_workers = 50) as t:
            t.map(keren, cursor.fetchall())
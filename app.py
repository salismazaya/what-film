import pinecone, base64, requests as r
from io import BytesIO
import streamlit as st, base64

pinecone.init(
    api_key = '10119962-3d62-4e4e-91ea-8bd1ed73d774',
    environment = 'us-central1-gcp'
)

index = pinecone.Index('what-index')

def predict(image):
    payload = {"image": image}
    response = r.post(
        'http://127.0.0.1:8000/embedding', json = payload
    )
    rv = response.json()
    return rv


uploaded_file = st.file_uploader("Choose a image")
if uploaded_file is not None:
    image_b = uploaded_file.getvalue()
    image = base64.b64encode(image_b).decode()
    embed = predict(image)

    data = index.query(embed, top_k = 1)['matches'][0]
    name, timestamp = data['id'].split('_', -1)
    timestamp = int(timestamp)
    hours = timestamp // 3600
    minutes = timestamp % 3600 // 60
    seconds = (timestamp % 3600) % 60
    st.image(BytesIO(image_b), caption = f'{name} {hours}:{minutes}:{seconds}')